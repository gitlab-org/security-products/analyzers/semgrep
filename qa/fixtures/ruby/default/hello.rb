def some_rails_controller
  foo = params[:some_regex]
  Regexp.new(foo).match("some_string")
end

def use_params_in_regex
  @x = something.match /#{params[:x]}/
end

def regex_on_params
  @x = params[:x].match /foo/
end

some_rails_controller

# Add some extra lines to allow testing the --max-target-bytes CLI option:

# Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
# doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
# veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
# ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
# consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque
# porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
# adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et
# dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis
# nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex
# ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea
# voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem
# eum fugiat quo voluptas nulla pariatur?
