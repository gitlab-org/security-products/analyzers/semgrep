import express, { Request, Response } from "express";

const router = express.Router();

// http://localhost:3000/xss/express/0?name=%3Cscript%3Ealert(%27Hi%27)%3C/script%3E
router.get('/0', (req: Request, res: Response) => {
    const { name } = req.query;

    // Declare and initialize randomobj correctly
    const randomobj = {
        send: (message: string) => {
            console.log("randomobj message:", message);
        }
    };

    randomobj.send(`<h1> Hello : ${name} </h1>`);
    res.send(`<h1> Hello : ${name} </h1>`);
});

export default router;
