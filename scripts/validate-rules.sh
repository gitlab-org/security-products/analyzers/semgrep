#!/usr/bin/env sh

RULES_PATH="$1"

if [ ! -d "$RULES_PATH" ]; then
    echo "$RULES_PATH does not exist"
    exit 1
fi

ERROR=0
for rulefile in $(find "$RULES_PATH" -name '*.yml'); do
    semgrep --metrics=off --validate --config "$rulefile"
    [ $? -eq 0 ] || ERROR=1
done

exit $ERROR
