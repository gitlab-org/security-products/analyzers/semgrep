package rules_test

import (
	"bytes"
	"fmt"
	"path"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/semgrep/v5/rules"
)

func TestParseManifest(t *testing.T) {
	manifestEntries, err := rules.ParseManifest("../testdata/manifest/manifest.json", "../testdata")
	require.NoError(t, err)
	require.Len(t, manifestEntries, 5)

	for file, entry := range map[string]rules.ManifestEntry{
		"all files": {
			File:     "all files",
			Checksum: "ba072cbb9897445827b85b9b61158a1cf39642ce1eae659185d96ca991570b17",
		},
		"../testdata/gosec.yml": {
			File:     "dist/gosec.yml",
			Checksum: "48d730d251c3e37d19b4cb5b1f3dbd3188a75f7681bd2ea5176049a9dd6f1816",
		},
		"../testdata/lgpl/nodejs_scan.yml": {
			File:     "dist/lgpl/nodejs_scan.yml",
			Checksum: "e91a76830579cceb9b73d2bb6993b3b6be51191225112c573bc7d951f6708125",
		},
		"../testdata/security_code_scan.yml": {
			File:     "dist/security_code_scan.yml",
			Checksum: "8ed829dbd91fe1fc5c3106393aaae56f18cf38a0de982752316a0855b5c8389f",
		},
		"../testdata/unknown.yml": {
			File:     "dist/unknown.yml",
			Checksum: "xed829dbd91fe1fc5c3106393aaae56f18cf38a0de982752316a0855b5c8389f",
		},
	} {
		if _, ok := manifestEntries[file]; !ok {
			require.Fail(t, fmt.Sprintf("file %s not included in map", file))
		}

		require.Equal(t, entry, manifestEntries[file])
	}
}

func TestStatsWithManifest(t *testing.T) {
	var buffer bytes.Buffer
	dummyLogger := logrus.New()
	dummyLogger.SetFormatter(&logrus.TextFormatter{
		DisableColors:    true,
		DisableTimestamp: true,
	})

	dummyLogger.SetOutput(&buffer)

	stats := rules.NewAnalysisStats(dummyLogger, "../testdata/manifest")
	err := stats.Compute()
	require.NoError(t, err)

	require.Equal(t, `level=info msg="3 active rule files detected with 52 active rules"
level=info msg=" * rule file '../testdata/manifest/gosec.yml': '48d730d251c3e37d19b4cb5b1f3dbd3188a75f7681bd2ea5176049a9dd6f1816'"
level=info msg=" * rule file '../testdata/manifest/lgpl/nodejs_scan.yml': '16e65108f43ba78d85caee26f096f75910caa081c8069d78985f807fff59e1fe'"
level=info msg=" * rule file '../testdata/manifest/new_rule.yml': '8ed829dbd91fe1fc5c3106393aaae56f18cf38a0de982752316a0855b5c8389f'"
level=info msg="Combined rule checksum: '8faac115b23ab4e9553884a20aee7acca118d1753912181b95a108f78e962aef'"
level=info msg="Using a customized ruleset"
level=info msg="  * Modified rule files:"
level=info msg="     - rule file 1: ../testdata/manifest/lgpl/nodejs_scan.yml"
level=info msg="  * Added rule files:"
level=info msg="     - rule file 1: ../testdata/manifest/new_rule.yml"
level=info msg="  * Deleted rule files:"
level=info msg="     - rule file 1: ../testdata/manifest/security_code_scan.yml"
level=info msg="     - rule file 2: ../testdata/manifest/unknown.yml"
`, buffer.String())
}

func TestBuildRuleMapWithFullPath(t *testing.T) {
	expect := map[string]map[string]bool{
		"../testdata/sampledist/bandit.yml": {
			"bandit.B303-1": true,
			"bandit.B101-1": true,
		},
		"../testdata/sampledist/eslint.yml": {
			"eslint.detect-no-csrf-before-method-override-1": true,
			"eslint.detect-non-literal-require-1":            true,
		},
		"../testdata/sampledist/find_sec_bugs.yml": {
			"find_sec_bugs.DMI_EMPTY_DB_PASSWORD-1.HARD_CODE_PASSWORD-2": true,
		},
		"../testdata/sampledist/flawfinder.yml": {
			"flawfinder.char-1.TCHAR-1.wchar_t-1": true,
		},
		"../testdata/sampledist/gosec.yml": {
			"gosec.G107-1": true,
		},
		"../testdata/sampledist/security_code_scan.yml": {
			"security_code_scan.SCS0005-1":           true,
			"security_code_scan.SCS0026-1.SCS0031-1": true,
		},
	}

	defaultConfigPath := path.Join("../testdata", "sampledist")
	ruleMapFullPath, err := rules.BuildRuleMap(defaultConfigPath, true)
	require.NoError(t, err)

	for expectedFilename, expectedRuleIDs := range expect {
		require.Contains(t, ruleMapFullPath, expectedFilename)
		rule := ruleMapFullPath[expectedFilename]
		for _, r := range rule.Rules {
			require.Contains(t, expectedRuleIDs, r.ID)
		}
	}
}

func TestBuildRuleMap(t *testing.T) {
	expect := map[string]map[string]bool{
		"bandit": {
			"bandit.B303-1": true,
			"bandit.B101-1": true,
		},
		"eslint": {
			"eslint.detect-no-csrf-before-method-override-1": true,
			"eslint.detect-non-literal-require-1":            true,
		},
		"find_sec_bugs": {
			"find_sec_bugs.DMI_EMPTY_DB_PASSWORD-1.HARD_CODE_PASSWORD-2": true,
		},
		"flawfinder": {
			"flawfinder.char-1.TCHAR-1.wchar_t-1": true,
		},
		"gosec": {
			"gosec.G107-1": true,
		},
		"security_code_scan": {
			"security_code_scan.SCS0005-1":           true,
			"security_code_scan.SCS0026-1.SCS0031-1": true,
		},
	}

	defaultConfigPath := path.Join("../testdata", "sampledist")
	ruleMapFullPath, err := rules.BuildRuleMap(defaultConfigPath, false)
	require.NoError(t, err)

	for expectedFilename, expectedRuleIDs := range expect {
		require.Contains(t, ruleMapFullPath, expectedFilename)
		rule := ruleMapFullPath[expectedFilename]
		for _, r := range rule.Rules {
			require.Contains(t, expectedRuleIDs, r.ID)
		}
	}
}

func TestComputeRuleStats(t *testing.T) {
	var buffer bytes.Buffer
	dummyLogger := logrus.New()
	dummyLogger.SetFormatter(&logrus.TextFormatter{
		DisableColors:    true,
		DisableTimestamp: true,
	})

	dummyLogger.SetOutput(&buffer)

	stats := rules.NewAnalysisStats(dummyLogger, "../testdata/rules-without-clash")
	err := stats.Compute()
	require.NoError(t, err)

	require.Equal(t, 2, stats.NumActiveRuleFiles())
	require.Equal(t, 3, stats.NumActiveRuleIDs())
	require.Equal(t, 0, stats.NumRuleIDClashes())
	require.Equal(t, "d540cd83eeec29479dc1c45e3309e0661b7a3f0f43e56ffea1c2830216406b49", stats.CombinedChecksum)

	require.Equal(t, `level=info msg="2 active rule files detected with 3 active rules"
level=info msg=" * rule file '../testdata/rules-without-clash/rule-LDAPInjection.yml': 'efdc5ad932fe08a59cbf750a52004f83fce23b7fbb4ef598c10e013c8d2fdf51'"
level=info msg=" * rule file '../testdata/rules-without-clash/rule-concat-sqli.yml': '5027676311a38a90077875f78b1a5e7c95beda527567f56a36e8aa5835424a28'"
level=info msg="Combined rule checksum: 'd540cd83eeec29479dc1c45e3309e0661b7a3f0f43e56ffea1c2830216406b49'"
level=info msg="No manifest file specified"
`, buffer.String())

	buffer.Reset()

	stats = rules.NewAnalysisStats(dummyLogger, "../testdata/rules-with-clash")
	err = stats.Compute()

	require.NoError(t, err)
	require.Equal(t, 2, stats.NumActiveRuleFiles())
	require.Equal(t, 3, stats.NumActiveRuleIDs())
	require.Equal(t, 1, stats.NumRuleIDClashes())
	require.Equal(t, "0cb99f964edac82663f165eb1471044ca5889393879d0665a5d16bef42dd4a84", stats.CombinedChecksum)

	require.Equal(t, `level=warning msg="Rule ID java_inject_rule-MyLDAPInjection is used 2 times"
level=info msg="2 active rule files detected with 3 active rules"
level=info msg=" * rule file '../testdata/rules-with-clash/rule-LDAPInjection.yml': '099e2967d3fb5015f17fae5e46b47c44e5571bfe3c9302d890cde6248e9d4934'"
level=info msg=" * rule file '../testdata/rules-with-clash/rule-concat-sqli.yml': '5027676311a38a90077875f78b1a5e7c95beda527567f56a36e8aa5835424a28'"
level=info msg="Combined rule checksum: '0cb99f964edac82663f165eb1471044ca5889393879d0665a5d16bef42dd4a84'"
level=info msg="No manifest file specified"
`, buffer.String())
}
