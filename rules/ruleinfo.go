package rules

import (
	"crypto/sha256"
	"fmt"
	"io"
	"io/fs"
	"path"
	"sort"

	"gopkg.in/yaml.v2"

	"encoding/json"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

const manifestFile = "manifest.json"

type ruleFileStatus int

const (
	ruleAdded ruleFileStatus = iota
	ruleModified
	ruleDeleted
)

func (s ruleFileStatus) String() string {
	switch s {
	case ruleAdded:
		return "Added"
	case ruleModified:
		return "Modified"
	case ruleDeleted:
		return "Deleted"
	}
	return ""
}

// ParseManifest parses a manifest file
func ParseManifest(pathToManifest, replacementPath string) (map[string]ManifestEntry, error) {
	manifestEntries := []ManifestEntry{}
	ret := map[string]ManifestEntry{}

	byteValue, err := os.ReadFile(pathToManifest)
	if err != nil {
		return ret, err
	}

	if err := json.Unmarshal(byteValue, &manifestEntries); err != nil {
		return ret, err
	}

	for _, entry := range manifestEntries {
		splits := strings.Split(entry.File, string(os.PathSeparator))
		pathRewrite := []string{entry.File}
		if len(splits) > 1 {
			pathRewrite = append([]string{replacementPath}, splits[1:]...)
		}
		ret[filepath.Join(pathRewrite...)] = entry
	}

	return ret, nil
}

func (stats *AnalysisStats) computeManifestInfo() error {
	if len(stats.manifestFiles) == 0 {
		return nil
	}

	// iterate over the actual rule-set
	// if there is a corresponding entry in the manifest file -- compare
	// if there is no corresponding entry in the manifest file -- we have a
	// newly added file
	for file, checksum := range stats.ruleFileChecksums {
		if e, ok := stats.manifestFiles[file]; ok {
			if e.Checksum != checksum {
				stats.ruleFileStatus[ruleModified] = append(stats.ruleFileStatus[ruleModified], file)
			}
			continue
		}
		stats.ruleFileStatus[ruleAdded] = append(stats.ruleFileStatus[ruleAdded], file)
	}

	// iterate over all the files in the manifest
	// if there is a file in the manifest, that is not present in the
	// current rule-set, it has been deleted
	for file := range stats.manifestFiles {
		// all files is a placeholder for the combined checksum
		if file == "all files" {
			continue
		}
		if _, ok := stats.ruleFileChecksums[file]; !ok {
			stats.ruleFileStatus[ruleDeleted] = append(stats.ruleFileStatus[ruleDeleted], file)
		}
	}

	// deterministic output
	sort.Strings(stats.ruleFileStatus[ruleModified])
	sort.Strings(stats.ruleFileStatus[ruleAdded])
	sort.Strings(stats.ruleFileStatus[ruleDeleted])

	return nil
}

// Compute some rule statistics and meta-information that is
// made available through the AnalysisStats object
func (stats *AnalysisStats) Compute() error {
	ruleMap, err := BuildRuleMap(stats.rulepath, true)
	if err != nil {
		return err
	}

	var combinedString strings.Builder

	// identify clashing rule IDs
	// build list of rule IDs for searching
	// build list of rule files
	for ruleFile, rule := range ruleMap {
		stats.ruleFiles = append(stats.ruleFiles, ruleFile)
		for _, r := range rule.Rules {
			stats.ruleIDs[r.ID]++
		}
	}

	sort.Strings(stats.ruleFiles)

	// compute checksums for each rule file
	// compute checksum for all rule files
	for _, ruleFile := range stats.ruleFiles {
		checksumString, err := sha256FromFile(ruleFile)
		if err != nil {
			return err
		}

		stats.ruleFileChecksums[ruleFile] = checksumString
		combinedString.WriteString(checksumString)
		combinedString.WriteRune('\n')
	}

	combinedChecksum := sha256.Sum256([]byte(combinedString.String()))
	stats.CombinedChecksum = fmt.Sprintf("%x", combinedChecksum)

	manifestFileLocation := filepath.Join(stats.rulepath, manifestFile)
	if _, err := os.Stat(manifestFileLocation); err == nil {
		manifestFiles, err := ParseManifest(manifestFileLocation, stats.rulepath)
		if err != nil {
			log.Warnf("error while parsing manifest: %s", err)
		}
		stats.manifestFiles = manifestFiles
	}

	err = stats.computeManifestInfo()
	if err != nil {
		return err
	}

	stats.logStatus()

	return nil
}

type ruleFileStatusMap map[ruleFileStatus][]string

func (m ruleFileStatusMap) Empty() bool {
	return len(m[ruleAdded])+len(m[ruleModified])+len(m[ruleDeleted]) == 0
}

// AnalysisStats includes meta-information about semgrep rules
type AnalysisStats struct {
	CombinedChecksum  string
	ruleFileChecksums map[string]string
	ruleIDs           map[string]int
	ruleFiles         []string
	rulepath          string
	manifestFiles     map[string]ManifestEntry
	ruleFileStatus    ruleFileStatusMap
	logger            *log.Logger
}

// NewAnalysisStats generates a new stats object
func NewAnalysisStats(logger *log.Logger, rulepath string) AnalysisStats {
	return AnalysisStats{
		CombinedChecksum:  "",
		ruleFileChecksums: map[string]string{},
		ruleIDs:           map[string]int{},
		ruleFiles:         []string{},
		logger:            logger,
		rulepath:          rulepath,
		manifestFiles:     map[string]ManifestEntry{},
		ruleFileStatus: ruleFileStatusMap{
			ruleAdded:    []string{},
			ruleModified: []string{},
			ruleDeleted:  []string{},
		},
	}
}

// logStatus logs meta-information about the analysis
func (stats AnalysisStats) logStatus() {
	if len(stats.ruleFileChecksums) == 0 {
		stats.logger.Errorf("No active rules detected")
		return
	}

	for id, count := range stats.ruleIDs {
		if count > 1 {
			stats.logger.Warnf("Rule ID %s is used %d times", id, count)
		}
	}
	stats.logger.Infof("%d active rule files detected with %d active rules", len(stats.ruleFileChecksums), len(stats.ruleIDs))

	for _, file := range stats.ruleFiles {
		if _, ok := stats.ruleFileChecksums[file]; !ok {
			continue
		}
		stats.logger.Infof(" * rule file '%s': '%s'", file, stats.ruleFileChecksums[file])
	}
	stats.logger.Infof("Combined rule checksum: '%s'", stats.CombinedChecksum)

	// no manifest files are specified -- we can return; the logic below is
	// dedicated towards rule comparisons against the manifest file
	if len(stats.manifestFiles) == 0 {
		stats.logger.Info("No manifest file specified")
		return
	}

	if stats.ruleFileStatus.Empty() {
		stats.logger.Info("Using the GitLab SAST default ruleset")
		return
	}

	stats.logger.Info("Using a customized ruleset")

	// predicatble sorting order
	for _, status := range []ruleFileStatus{ruleModified, ruleAdded, ruleDeleted} {
		if _, ok := stats.ruleFileStatus[status]; !ok {
			continue
		}
		if len(stats.ruleFileStatus[status]) == 0 {
			continue
		}

		stats.logger.Infof("  * %s rule files:", status.String())
		for idx, file := range stats.ruleFileStatus[status] {
			stats.logger.Infof("     - rule file %d: %s", idx+1, file)
		}
	}
}

// NumActiveRuleFiles returns the number of active rules files for the current
// semgrep run
func (stats AnalysisStats) NumActiveRuleFiles() int {
	return len(stats.ruleFileChecksums)
}

// NumActiveRuleIDs returns the ids of the rules that are currently active
func (stats AnalysisStats) NumActiveRuleIDs() int {
	return len(stats.ruleIDs)
}

// NumRuleIDClashes returns the number of rule id clashes
func (stats AnalysisStats) NumRuleIDClashes() int {
	c := 0
	for _, count := range stats.ruleIDs {
		if count > 1 {
			c++
		}
	}
	return c
}

func sha256FromFile(filepath string) (string, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := sha256.New()
	if _, err = io.Copy(hash, file); err != nil {
		return "", err
	}
	checksum := hash.Sum(nil)
	return fmt.Sprintf("%x", string(checksum)), nil
}

// BuildRuleMap creates a map that includes filenames and their mapping to the
// corresponding semgrep rule file object
func BuildRuleMap(configPath string, useFullPath bool) (map[string]SemgrepRuleFile, error) {
	ruleMap := map[string]SemgrepRuleFile{}

	return ruleMap, filepath.WalkDir(configPath, func(p string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if _, err = os.Stat(p); err != nil || d.IsDir() {
			return err
		}

		ext := path.Ext(p)
		if ext != ".yml" && ext != ".yaml" {
			log.Debugf("skipping parse for non-rule file: %s", p)
			return nil
		}

		var ruleFile SemgrepRuleFile

		fileContent, err := os.ReadFile(p)
		if err != nil {
			return fmt.Errorf("read rule file at %s: %w", p, err)
		}

		if err = yaml.Unmarshal(fileContent, &ruleFile); err != nil {
			return fmt.Errorf("parse rule file at %s: %w", p, err)
		}

		if useFullPath {
			ruleMap[p] = ruleFile
		} else {
			rulesetFile := strings.Split(filepath.Base(p), ".")[0]
			ruleMap[rulesetFile] = ruleFile
		}

		return nil
	})
}
